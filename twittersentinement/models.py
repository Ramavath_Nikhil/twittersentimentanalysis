# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
import json


# Create your models here.

class Tweet(models.Model):
    polarity = models.TextField()
    subjectivity = models.TextField()
    text = models.TextField()

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)
